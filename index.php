<?php

require 'vendor/autoload.php';

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();
$dotenv->required(array('DB_HOST', 'DB_NAME', 'DB_USER', 'DB_PASS'));

// configure system
$f3 = \Base::instance();

$f3->set('AUTOLOAD', 'app/');
$f3->set('DEBUG', 2);
$f3->set('UPLOADS', 'tmp/uploads/');
$f3->set('ASSETS', 'public/');

// set up Twig templates
$twig = new Twig_Environment(new Twig_Loader_Filesystem('app/Views'), [
    'auto_reload' => true,
    'debug' => true,
    'cache' => 'tmp/cache'
]);

$twig->addExtension(new Twig_Extensions_Extension_Text());

$twig->addFunction(new Twig_SimpleFunction('path', function($name, $args = null) {
    $f3 = \Base::instance();
    return $f3->get('BASE') . rtrim($f3->alias($name, $args), '/');
}));

$twig->addFunction(new Twig_SimpleFunction('asset', function($path) {
    $f3 = \Base::instance();
    return $f3->get('BASE') . '/' . trim($f3->get('ASSETS'), '/') . '/' . ltrim($path, '/');
}));

$f3->set('TWIG', $twig);

// configure database
$db = $f3->set('DB', new DB\SQL(
    'mysql:host=' . getenv('DB_HOST') . ';port=' . getenv('DB_PORT') . ';dbname=' . getenv('DB_NAME'),
    getenv('DB_USER'),
    getenv('DB_PASS'),
    array(
        \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4',
    )
));

new \DB\SQL\Session($db, getenv('DB_PREFIX') . 'sessions');

if ($f3->exists('SESSION.userId')) {
    $user = new \Models\User();
    $user->load($f3->get('SESSION.userId'));

    if (!$user->dry()) {
        $f3->set('USER', $user);
    }
}

if ($f3->exists('SESSION.flashes')) {
    $f3->copy('SESSION.flashes', 'flashes');
    $f3->clear('SESSION.flashes');
}

$f3->set('ONREROUTE', function() use ($f3) {
    if ($f3->exists('flashes')) {
        $f3->copy('flashes', 'SESSION.flashes');
    }

    return false;
});

// define navigation
$nav = new \Navigation();

if ($f3->exists('USER')) {
    $nav->addItem('/', 'Bestellungen');
    $nav->addItem('/admin', 'Verwaltung');
    $nav->addItem('/profile', 'Profil', array('icon' => 'fa-user-circle', 'icon_only' => true));
    $nav->addItem('/logout', 'Abmelden', array('icon' => 'fa-sign-out', 'icon_only' => true));
}

$f3->set('nav', $nav);

// define routes
if ($f3->exists('USER')) {
    $f3->route('GET @home:/', 'Controllers\Home->index');
    $f3->route('GET @logout:/logout', 'Controllers\Auth->logout');

    $f3->route('GET @cashregister:/admin', 'Controllers\Admin\CashRegister->indexAction');
    $f3->route('GET @suppliers_list:/admin/suppliers', 'Controllers\Admin\Suppliers->listAction');
    $f3->route('GET @suppliers_show:/admin/suppliers/@id', 'Controllers\Admin\Suppliers->showAction');
    $f3->route('GET|POST @suppliers_new:/admin/suppliers/new', 'Controllers\Admin\Suppliers->newAction');
    $f3->route('GET|POST @suppliers_edit:/admin/suppliers/@id/edit', 'Controllers\Admin\Suppliers->editAction');
    $f3->route('GET|POST @suppliers_delete:/admin/suppliers/@id/delete', 'Controllers\Admin\Suppliers->deleteAction');
    $f3->route('GET|POST @users_list:/admin/users', 'Controllers\Admin\Users->listAction');

    $f3->route('GET @profile:/profile', 'Controllers\Profile->form');
    $f3->route('POST /profile', 'Controllers\Profile->save');
} else {
    $f3->route('GET @home:/', function($f3) { $f3->reroute('/login'); });
    $f3->route('GET|POST @login:/login', 'Controllers\Auth->login', 0, 64);
    $f3->route('GET|POST @register:/register', 'Controllers\Auth->register', 0, 64);
}

// start system
$f3->run();
