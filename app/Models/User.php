<?php

namespace Models;

class User extends \Model
{
    protected $table = 'users';

    public function setPassword($password)
    {
        $crypt = \Bcrypt::instance();
        $this->password = $crypt->hash($password);
    }

    public function verifyPassword($password)
    {
        // support unix crypt(3) hash algorithm
        if (strlen($this->password) < 14) {
            return crypt($password, $this->password) == $this->password;
        }

        // support apache md5 hash algorithm
        if (strpos($this->password, '$apr1$') === 0) {
            return \WhiteHat101\Crypt\APR1_MD5::check($password, $this->password);
        }

        $crypt = \Bcrypt::instance();
        return $crypt->verify($password, $this->password);
    }

    public function hasWeakPasswordHash() {
        $crypt = \Bcrypt::instance();
        return $crypt->needs_rehash($this->password);
    }
}
