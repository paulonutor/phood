<?php

abstract class Model extends \DB\SQL\Mapper
{
    public function __construct() {
        $f3 = \Base::instance();

        $db = $f3->get('DB');
        $tableName = getenv('DB_PREFIX') . $this->table;

        parent::__construct($db, $tableName);
    }

    public function only($fields) {
        $this->fields = array_intersect_key($this->fields, array_flip($fields));
        return $this;
    }

    public function load($filter=null, array $options = null, $ttl = 0) {
        // support loading model by id
        if (is_numeric($filter)) {
            return parent::load(array("id = ?", $filter), $options, $ttl);
        }

        return parent::load($filter, $options, $ttl);
    }

    public function update()
    {
        // update 'updated_at' with current timestamp if any exists
        if ($this->changed() && array_key_exists('updated_at', $this->fields)) {
            $this->updated_at = date('Y-m-d H:i:s');
        }

        return parent::update();
    }
}
