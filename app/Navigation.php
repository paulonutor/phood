<?php

class Navigation
{
    protected $items = array();

    public function __construct($root = '') {
        $this->root = rtrim($root, '/');
    }

    public function addItem($url, $title, $options = array())
    {
        $this->items[] = array_merge($options, array(
            'url' => rtrim($url, '/'),
            'title' => $title
        ));
    }

    public function getItems()
    {
        $f3 = \Base::instance();
        $base = $f3->get('BASE');
        $path = $f3->get('PATH');
        $rootItem = null;
        $result = array();

        foreach ($this->items as $item) {
            $url = $item['url'];
            $isActive = preg_match('#^' . preg_quote($url, '#') . '#', $path) == 1;

            $navItem = array_merge($item, array(
                'url' => $base . $url,
                'is_active' => $isActive
            ));

            if ($isActive && $rootItem && $rootItem['is_active']) {
                $rootItem['is_active'] = false;
            }

            if ($url == $this->root) {
                $rootItem = & $navItem;
            }

            $result[] = & $navItem;
            unset($navItem);
        }

        return $result;
    }
}
