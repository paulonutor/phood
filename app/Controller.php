<?php

abstract class Controller
{
    protected function render($template, array $data = array())
    {
        $className = get_class($this);
        $controllerName = explode('\\', $className, 2)[1];

        $f3 = \Base::instance();
        $allData = array_merge($f3->hive(), $data);

        echo $f3->get('TWIG')->render($controllerName . '/' . $template . '.twig', $allData);
    }

    protected function isPost()
    {
        $f3 = \Base::instance();
        return $f3->get('VERB') == 'POST';
    }

    protected function addFlash($category, $message)
    {
        $f3 = \Base::instance();
        $f3->push('flashes', array(
            'category' => $category,
            'message' => $message
        ));
    }
}
