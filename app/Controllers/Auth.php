<?php

namespace Controllers;

class Auth extends \Controller
{
    public function register($f3)
    {
        if ($this->isPost()) {
            $post = $f3->get('POST');
            $trimmedPost = array_map(array(\UTF::instance(), 'trim'), $f3->get('POST'));

            if (empty($trimmedPost['fullname'])) {
                $errors['fullname'] = 'Bitte gebe deinen Namen ein!';
            }

            if (empty($trimmedPost['username'])) {
                $errors['username'] = 'Bitte gebe einen Benutzernamen ein!';
            } elseif (!preg_match('/^[a-z_][a-z0-9_-]{0,29}$/', $trimmedPost['username'])) {
                $errors['username'] = <<<'EOT'
Der Benutzernamen darf nur Buchstaben, Zahlen, Unterstriche oder Bindestriche enthalten und muss mit einem Buchstaben beginnnen.

Maximal sind 30 Zeichen erlaubt.
EOT;
            } elseif ((new \Models\User())->find(array('username = ?', $trimmedPost['username']))) {
                $errors['username'] = 'Ein Benutzer mit diesem Benutzernamen existiert bereits.';
            }

            if (empty($post['password'])) {
                $errors['password'] = 'Bitte gebe ein Passwort ein!';
            } elseif (mb_strlen($post['password']) < 6) {
                $errors['password'] = 'Das Passwort sollte mindestens 6 Zeichen lang sein.';
            }

            if (!empty($trimmedPost['email']) && !filter_var($trimmedPost['email'], FILTER_VALIDATE_EMAIL)) {
                $errors['email'] = 'Das ist keine valide E-Mail-Adresse.';
            }

            if (empty($errors)) {
                $user = new \Models\User();
                $user->username = mb_strtolower(filter_var($trimmedPost['username'], FILTER_SANITIZE_STRING));
                $user->fullname = filter_var($trimmedPost['fullname'], FILTER_SANITIZE_STRING);
                $user->email = $trimmedPost['email'];
                $user->ircnick = $trimmedPost['ircnick'];
                $user->phone = $trimmedPost['phone'];

                try {
                    $user->setPassword($post['password']);
                    $user->save();

                    $this->addFlash('success', 'Du wurdest erfolgreich registriert. Nun muss dich nur noch ein Admin freischalten.');
                    $f3->reroute('@login');
                } catch (\Exception $err) {
                    $this->addFlash('error', 'Fehler bei der Registrierung aufgetreten.');
                }
            } else {
                $f3->set('errors', $errors);
            }
        }

        $this->render('register');
    }

    public function login($f3)
    {
        $model = new \Models\User();

        if ($this->isPost()) {
            $post = $f3->get('POST');
            $username = trim($post['username']);
            $password = $post['password'];
            $errors = array();

            if (empty($username)) {
                $errors['username'] = 'Bitte einen Benutzernamen eingeben!';
            }

            if (empty($password)) {
                $errors['password'] = 'Bitte ein Passwort eingeben!';
            }

            if (empty($errors)) {
                $user = new \Models\User();
                $user->only(array('id', 'password', 'is_approved', 'lastlogin_on'));
                $user->load(array('username = ? AND is_hidden = 0', $username));

                if ($user->dry() || !$user->verifyPassword($password)) {
                    $this->addFlash('error', 'Falscher Benutzername oder Passwort.');
                } elseif (!$user->is_approved) {
                    $this->addFlash('error', 'Du wurdest noch nicht freigeschaltet.');
                } else {
                    if ($user->hasWeakPasswordHash()) {
                        $user->setPassword($password);
                    }

                    $user->lastlogin_on = date('Y-m-d H:i:s');
                    $user->save();

                    $f3->clear('SESSION');
                    $f3->set('SESSION.userId', $user->id);
                    $f3->reroute('@home');
                }
            } else {
                $f3->set('errors', $errors);
            }
        }

        $this->render('login');
    }

    public function logout($f3)
    {
        $f3->clear('SESSION');
        $f3->reroute('@home');
    }
}
