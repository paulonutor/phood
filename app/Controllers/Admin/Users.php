<?php

namespace Controllers\Admin;

class Users extends Base
{
    public function listAction($f3)
    {
        $currentUser = $f3->get('USER');
        $db = $f3->get('DB');

        $users = (new \Models\User())->find(null, array('order' => 'fullname, username'));
        $approvedUsers = array_filter($users, function($user) { return $user->is_approved; });
        $unapprovedUsers = array_filter($users, function($user) { return !$user->is_approved; });

        if ($this->isPost()) {
            if (!$currentUser->is_admin) {
                $f3->error(403);
            }

            $post = $f3->get('POST');

            $db->begin();

            foreach ($approvedUsers as $user) {
                if (array_key_exists($user->id, $post)) {
                    $postData = $post[$user->id];
                    $user->phone = trim($postData['phone']);
                    $user->is_hidden = $postData['is_hidden'] == 'on';

                    if ($user->id != $currentUser->id) {
                        $user->is_admin = $postData['is_admin'] == 'on';
                    }

                    $user->is_treasurer = $postData['is_treasurer'] == 'on';
                    $user->can_order = $postData['can_order'] == 'on';

                    $user->update();
                }
            }

            foreach ($unapprovedUsers as $index => $user) {
                if (array_key_exists($user->id, $post)) {
                    $postData = $post[$user->id];
                    if (array_key_exists('approve', $postData)) {
                        $user->is_approved = 1;
                        $user->update();
                    } elseif (array_key_exists('decline', $postData)) {
                        $user->erase();
                        unset($unapprovedUsers[$index]);
                    }
                }
            }

            $db->commit();
        }

        $f3->set('approved_users', $approvedUsers);
        $f3->set('unapproved_users', $unapprovedUsers);

        $this->render('list');
    }
}
