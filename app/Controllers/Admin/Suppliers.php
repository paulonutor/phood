<?php

namespace Controllers\Admin;

class Suppliers extends Base
{
    public function listAction($f3)
    {
        $f3->set('suppliers', (new \Models\Supplier())->find(null, array('order' => 'name')));
        $this->render('list');
    }

    public function showAction($f3, $params)
    {
        $supplier = new \Models\Supplier();
        $supplier->load($params['id']);

        if ($supplier->dry()) {
            $f3->error(404);
        }

        $f3->set('supplier', $supplier);
        $this->render('show');
    }

    public function newAction($f3)
    {
        $currentUser = $f3->get('USER');

        if (!$currentUser->is_admin) {
            $f3->error(403);
        }

        if ($this->isPost()) {
            $this->saveSupplier($f3, new \Models\Supplier());
        }

        $this->render('form');
    }

    public function editAction($f3, $params)
    {
        $currentUser = $f3->get('USER');

        if (!$currentUser->is_admin) {
            $f3->error(403);
        }

        $supplier = new \Models\Supplier();
        $supplier->load($params['id']);

        if ($supplier->dry()) {
            $f3->error(404);
        }

        if ($this->isPost()) {
            $this->saveSupplier($f3, $supplier);
        } else {
            $supplier->copyto('POST');
        }

        $f3->set('is_editing', true);
        $f3->set('supplier_id', $supplier->id);
        $this->render('form');
    }

    public function deleteAction($f3, $params)
    {
        $currentUser = $f3->get('USER');

        if (!$currentUser->is_admin) {
            $f3->error(403);
        }

        $supplier = new \Models\Supplier();
        $supplier->load($params['id']);

        if ($supplier->dry()) {
            $f3->error(404);
        }

        if ($this->isPost()) {
            $supplier->erase();

            $this->addFlash('success', 'Lieferant wurde erfolgerich gelöscht.');
            $f3->reroute('@suppliers_list');
        }

        $f3->set('supplier', $supplier);
        $this->render('delete');
    }

    private function save($f3, $supplier)
    {
        $post = array_map(array(\UTF::instance(), 'trim'), $f3->get('POST'));

        if (empty($post['name'])) {
            $errors['name'] = 'Bitte gebe einen Namen ein!';
        }

        if (!empty($post['website']) && !filter_var($post['website'], FILTER_VALIDATE_URL)) {
            $errors['website'] = 'Das ist keine valide URL.';
        }

        if (empty($errors)) {
            $supplier->name = filter_var($post['name'], FILTER_SANITIZE_STRING);
            $supplier->website = filter_var($post['website'], FILTER_SANITIZE_STRING);
            $supplier->phone = filter_var($post['phone'], FILTER_SANITIZE_STRING);
            $supplier->fax = filter_var($post['fax'], FILTER_SANITIZE_STRING);
            $supplier->remarks = $post['remarks'];
            $supplier->can_deliver = $post['can_deliver'] == 'on';
            $supplier->is_hidden = $post['is_hidden'] == 'on';

            $supplier->save();

            $this->addFlash('success', 'Lieferant wurde erfolgerich gespeichert.');
            $f3->reroute('@suppliers_show', array('id' => $supplier->id));
        } else {
            $f3->set('errors', $errors);
        }
    }
}
