<?php

namespace Controllers\Admin;

class Base extends \Controller
{
    public function __construct()
    {
        $nav = new \Navigation('/admin');
        $nav->addItem('/admin', 'Kasse');
        $nav->addItem('/admin/suppliers', 'Lieferanten');
        $nav->addItem('/admin/users', 'Benutzer');

        $f3 = \Base::instance();
        $f3->set('subnav', $nav);
    }
}
