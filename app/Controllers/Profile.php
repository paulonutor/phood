<?php

namespace Controllers;

class Profile extends \Controller
{
    public function form($f3)
    {
        $user = $f3->get('USER');
        $user->copyto('POST');

        $this->render('form');
    }

    public function save($f3)
    {
        $user = $f3->get('USER');

        if (array_key_exists('password', $f3->get('POST'))) {
            $this->savePassword($user, $f3);
            $user->copyto('POST');
        } else {
            $this->saveProfile($user, $f3);
        }

        $this->render('form');
    }

    private function saveProfile($user, $f3) {
        $post = array_map(array(\UTF::instance(), 'trim'), $f3->get('POST'));

        if (empty($post['username'])) {
            $errors['username'] = 'Bitte gebe einen Benutzernamen ein!';
        } elseif (!preg_match('/^[a-z_][a-z0-9_-]{0,29}$/', $post['username'])) {
            $errors['username'] = <<<'EOT'
Der Benutzernamen darf nur Buchstaben, Zahlen, Unterstriche oder Bindestriche enthalten und muss mit einem Buchstaben beginnnen.

Maximal sind 30 Zeichen erlaubt.
EOT;
        } elseif ($post['username'] != $user->username && (new \Models\User())->find(array('username = ?', $post['username']))) {
            $errors['username'] = 'Ein Benutzer mit diesem Benutzernamen existiert bereits.';
        }

        if (empty($post['fullname'])) {
            $errors['fullname'] = 'Bitte gebe deinen Namen ein!';
        }

        if (!empty($post['email']) && !filter_var($post['email'], FILTER_VALIDATE_EMAIL)) {
            $errors['email'] = 'Das ist keine valide E-Mail-Adresse.';
        }

        if (empty($errors)) {
            $user->username = mb_strtolower(filter_var($post['username'], FILTER_SANITIZE_STRING));
            $user->fullname = filter_var($post['fullname'], FILTER_SANITIZE_STRING);
            $user->email = $post['email'];
            $user->ircnick = $post['ircnick'];
            $user->phone = $post['phone'];

            try {
                $user->save();
                $this->addFlash('success', 'Die Daten wurden erfolgreich gespeichert.');
                $user->copyto('POST');
            } catch (\Exception $err) {
                $this->addFlash('error', 'Fehler beim Speichern der Daten.');
            }
        } else {
            $f3->set('errors', $errors);
        }
    }

    private function savePassword($user, $f3) {
        $post = $f3->get('POST');
        $errors = array();

        if (empty($post['password'])) {
            $errors['password'] = 'Bitte gebe ein neues Passwort ein!';
        } elseif (mb_strlen($post['password']) < 6) {
            $errors['password'] = 'Das Passwort sollte mindestens 6 Zeichen lang sein.';
        }

        if ($post['password'] != $post['password_confirm']) {
            $errors['password_confirm'] = 'Die Passwörter unterscheiden sich.';
        }

        if (empty($errors)) {
            try {
                $user->setPassword($post['password']);
                $user->save();
                $this->addFlash('success', 'Das Passwort wurde erfolgreich geändert.');
            } catch (\Exception $err) {
                $this->addFlash('error', 'Fehler beim Ändern des Passwortes.');
            }
        } else {
            $f3->set('errors', $errors);
        }
    }
}
