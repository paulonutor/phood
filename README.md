Phood
=====

This is a food ordering system.

Installation
------------

```
bin/composer.phar install

cp .env.sample .env
# edit ".env" to match your local MySQL settings

vendor/bin/phinx migrate
```
