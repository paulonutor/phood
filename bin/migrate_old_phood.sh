#!/bin/sh

test -z $1 && echo "the old database name must be provided as first argument." 1>&2 && exit 1
test -z $2 && echo "a path to the old phood data must be provided as second argument." 1>&2 && exit 1

OLD_DB="$1"
OLD_PATH="$2"

shift
source .env

PREFIX="$DB_NAME.$DB_PREFIX"
MYSQL_CMD='MYSQL_PWD="$DB_PASS" mysql -h"$DB_HOST" -P"$DB_PORT" -u"$DB_USER" -D"$DB_NAME"'

SQL=$(cat <<EOT
    SET sql_mode='';

    DELETE FROM {{PREFIX}}orderitem_modifiers;
    DELETE FROM {{PREFIX}}orderitems;
    DELETE FROM {{PREFIX}}orders;
    DELETE FROM {{PREFIX}}menuitem_modifiers;
    DELETE FROM {{PREFIX}}menuitems;
    DELETE FROM {{PREFIX}}flyers;
    DELETE FROM {{PREFIX}}suppliers;
    DELETE FROM {{PREFIX}}payments;
    DELETE FROM {{PREFIX}}paymenttokens;
    DELETE FROM {{PREFIX}}users;

    -- old password mapping
    CREATE TEMPORARY TABLE tmp_passwd_mapping (
        id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        username VARCHAR(255) NOT NULL,
        password VARCHAR(255) NOT NULL
    );

    LOAD DATA LOCAL INFILE '{{OLD_PATH}}/.htpasswd'
    INTO TABLE tmp_passwd_mapping
    FIELDS TERMINATED BY ':'
    (username, password);

    CREATE TEMPORARY TABLE tmp_passwd_mapping2 AS
    SELECT MAX(id) AS id, username FROM tmp_passwd_mapping
    GROUP BY username;

    -- users
    INSERT IGNORE INTO {{PREFIX}}users (
        id,
        username,
        password,
        fullname,
        phone,
        balance,
        is_hidden,
        is_approved,
        is_admin,
        is_treasurer,
        can_order
    )
    SELECT
        u.id,
        LOWER(u.remote_user),
        p.password,
        u.name,
        u.direct_dial,
        COALESCE(o.balance, 0),
        u.hidden,
        1,
        u.treasurer,
        u.treasurer,
        u.approved
    FROM {{OLD_DB}}.users AS u
    INNER JOIN {{OLD_DB}}.overview o ON o.id = u.id
    LEFT JOIN tmp_passwd_mapping2 p2 ON p2.username = u.remote_user
    LEFT JOIN tmp_passwd_mapping p ON p.id = p2.id
    ;

    -- paymenttokens
    INSERT INTO {{PREFIX}}paymenttokens (id, token, value)
    SELECT id, token, amount FROM {{OLD_DB}}.payment_tokens;

    -- payments
    INSERT INTO {{PREFIX}}payments (user_id, paymenttoken_id, amount, created_at)
    SELECT t.uid, pt.id, d.amount, d.date
    FROM {{OLD_DB}}.t_deposit d
    LEFT JOIN {{OLD_DB}}.transactions t ON t.id = d.tid
    LEFT JOIN {{OLD_DB}}.payment_tokens pt ON pt.tid = d.tid
    ORDER BY d.tid
    ;

    -- suppliers
    INSERT INTO {{PREFIX}}suppliers (
        name,
        remarks,
        website,
        phone,
        fax,
        is_hidden,
        can_deliver
    )
    SELECT
        name,
        text,
        website,
        phone,
        fax,
        hidden,
        delivery
    FROM (
        SELECT MAX(id) AS id
        FROM {{OLD_DB}}.suppliers
        GROUP BY name
    ) g
    JOIN {{OLD_DB}}.suppliers s ON s.id = g.id
    ;

    -- temporary supplier mapping
    CREATE TEMPORARY TABLE tmp_supplier_mapping AS
    SELECT s.id AS id, os.id AS old_id
    FROM {{OLD_DB}}.suppliers os
    JOIN {{PREFIX}}suppliers s ON s.name = os.name
    ;

    -- flyers
    INSERT INTO {{PREFIX}}flyers (
        supplier_id,
        ordering,
        filename,
        url
    )
    SELECT
        s.id,
        (CASE f.sid WHEN @curType THEN @curRow := @curRow + 1 ELSE @curRow := 1 AND @curType := f.sid END),
        IF(f.filename LIKE 'http%', NULL, f.filename),
        IF(f.filename LIKE 'http%', f.filename, NULL)
    FROM
        (SELECT @curRow := 0, @curType := '') r,
        {{OLD_DB}}.flyers f
    JOIN {{OLD_DB}}.suppliers os ON os.id = f.sid
    JOIN tmp_supplier_mapping s ON s.old_id = f.sid
    WHERE os.hidden = 0
    ORDER BY f.sid, f.filename
    ;

    -- menuitems
    INSERT INTO {{PREFIX}}menuitems (
        supplier_id,
        number,
        description,
        price,
        is_modfier
    )
    SELECT
        s.id,
        m.number,
        m.description,
        m.price,
        IF(m.number LIKE '~%', 1, m.modifier)
    FROM {{OLD_DB}}.menu m
    JOIN {{OLD_DB}}.suppliers os ON os.id = m.sid AND os.hidden = 0
    JOIN tmp_supplier_mapping s ON s.old_id = os.id
    WHERE m.hidden = 0
      AND m.combined = 0
      AND NOT EXISTS (SELECT 1 FROM {{OLD_DB}}.combinations c WHERE c.combined_mid = m.id)
    ;

    -- orders
    INSERT IGNORE INTO {{PREFIX}}orders (
        id,
        supplier_id,
        total_amount,
        paid_amount,
        real_balance,
        is_ordered,
        is_closed,
        ordered_at,
        closed_at,
        reopened_at,
        created_at,
        updated_at
    )
    SELECT
        o.oid,
        s.id,
        COALESCE(o.price_sum, 0),
        o.invoice_amount,
        IF(o.balance > 0, o.balance, NULL),
        o.is_ordered,
        IF(o.d_closed IS NULL, 0, 1),
        o.d_ordered,
        o.d_closed,
        o.d_reopened,
        o.d_opened,
        COALESCE(o.d_closed, o.d_reopened, o.d_ordered, o.d_opened)
    FROM {{OLD_DB}}.order_info o
    JOIN tmp_supplier_mapping s ON s.old_id = o.sid
    ;

    -- orderitems
    INSERT INTO {{PREFIX}}orderitems (
        order_id,
        user_id,
        number,
        description,
        price,
        paid_price,
        created_at,
        updated_at
    )
    SELECT
        i.oid,
        i.uids,
        i.number,
        i.description,
        i.price_per_piece,
        round(i.price_per_piece / o.price_sum * o.invoice_amount, 4),
        o.d_opened,
        o.d_closed
    FROM {{OLD_DB}}.any_order i
    JOIN {{OLD_DB}}.order_info o ON o.oid = i.oid
    ;
EOT
)

echo "$SQL" | sed -e "s/{{OLD_DB}}/$OLD_DB/" -e "s#{{OLD_PATH}}#$OLD_PATH#" -e "s/{{PREFIX}}/$PREFIX/" | eval $MYSQL_CMD --local-infile -v

rm -rf public/flyers
mkdir -p public/flyers

flyers=$(eval $MYSQL_CMD -se "'SELECT filename FROM ${PREFIX}flyers WHERE filename IS NOT NULL'")

for flyer in $flyers
do
    cp -v "$OLD_PATH/scan/$flyer" public/flyers/
done
