<?php

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

return array(
    'paths' => array(
        'migrations' => 'db/migrations'
    ),
    'environments' => array(
        'default_migration_table' => getenv('DB_PREFIX') . 'migrationlog',
        'default_database' => 'runtime',
        'runtime' => array(
            'adapter' => 'mysql',
            'host' => getenv('DB_HOST'),
            'port' => getenv('DB_PORT') ?: '3306',
            'name' => getenv('DB_NAME'),
            'user' => getenv('DB_USER'),
            'pass' => getenv('DB_PASS'),
            'table_prefix' => getenv('DB_PREFIX'),
            'charset' => 'utf8mb4'
        )
    )
);
