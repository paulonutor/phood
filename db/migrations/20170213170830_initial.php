<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class Initial extends AbstractMigration
{
    public function change()
    {
        $this->table('users', array('collation' => 'utf8mb4_general_ci'))
            ->addColumn('username', 'string', array('limit' => 100))
            ->addColumn('password', 'string', array('limit' => 100))
            ->addColumn('fullname', 'string', array('null' => true))
            ->addColumn('email', 'string', array('null' => true))
            ->addColumn('ircnick', 'string', array('null' => true))
            ->addColumn('phone', 'string', array('limit' => 100, 'null' => true))
            ->addColumn('balance', 'decimal', array('precision' => 10, 'scale' => 4, 'default' => 0))
            ->addColumn('is_hidden', 'boolean', array('default' => false))
            ->addColumn('is_admin', 'boolean', array('default' => false))
            ->addColumn('is_treasurer', 'boolean', array('default' => false))
            ->addColumn('can_order', 'boolean', array('default' => false))
            ->addColumn('lastlogin_on', 'timestamp', array('null' => true))
            ->addTimestamps()
            ->addIndex(array('username'), array('unique' => true))
            ->create();

        $this->table('paymenttokens', array('collation' => 'utf8mb4_general_ci'))
            ->addColumn('token', 'string', array('limit' => 20))
            ->addColumn('value', 'decimal', array('precision' => 5, 'scale' => 2))
            ->addIndex(array('token'), array('unique' => true))
            ->create();

        $this->table('payments', array('collation' => 'utf8mb4_general_ci'))
            ->addColumn('user_id', 'integer')
            ->addColumn('paymenttoken_id', 'integer', array('null' => true))
            ->addColumn('amount', 'decimal', array('precision' => 5, 'scale' => 2))
            ->addColumn('created_at', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
            ->addForeignKey('user_id', 'users', 'id', array('delete' => 'RESTRICT', 'update' => 'CASCADE'))
            ->addForeignKey('paymenttoken_id', 'paymenttokens', 'id', array('delete' => 'SET_NULL', 'update' => 'CASCADE'))
            ->create();

        $this->table('suppliers', array('collation' => 'utf8mb4_general_ci'))
            ->addColumn('name', 'string')
            ->addColumn('remarks', 'text', array('null' => true))
            ->addColumn('website', 'string', array('null' => true))
            ->addColumn('phone', 'string', array('limit' => 100, 'null' => true))
            ->addColumn('fax', 'string', array('limit' => 100, 'null' => true))
            ->addColumn('is_hidden', 'boolean', array('default' => false))
            ->addColumn('can_deliver', 'boolean', array('default' => false))
            ->addTimestamps()
            ->create();

        $this->table('flyers', array('collation' => 'utf8mb4_general_ci'))
            ->addColumn('supplier_id', 'integer')
            ->addColumn('ordering', 'integer', array('limit' => MysqlAdapter::INT_TINY))
            ->addColumn('filename', 'string', array('null' => true))
            ->addColumn('url', 'string', array('null' => true))
            ->addForeignKey('supplier_id', 'suppliers', 'id', array('delete' => 'CASCADE', 'update' => 'CASCADE'))
            ->create();

        $this->table('menuitems', array('collation' => 'utf8mb4_general_ci'))
            ->addColumn('supplier_id', 'integer')
            ->addColumn('number', 'string', array('limit' => 30))
            ->addColumn('description', 'string')
            ->addColumn('price', 'decimal', array('precision' => 5, 'scale' => 2))
            ->addColumn('is_hidden', 'boolean', array('default' => false))
            ->addColumn('is_modfier', 'boolean', array('default' => false))
            ->addForeignKey('supplier_id', 'suppliers', 'id', array('delete' => 'CASCADE', 'update' => 'CASCADE'))
            ->create();

        $this->table('menuitem_modifiers', array('id' => false, 'primary_key' => array('menuitem_id', 'modifier_id'), 'collation' => 'utf8mb4_general_ci'))
            ->addColumn('menuitem_id', 'integer')
            ->addColumn('modifier_id', 'integer')
            ->addForeignKey('menuitem_id', 'menuitems', 'id', array('delete' => 'CASCADE', 'update' => 'CASCADE'))
            ->addForeignKey('modifier_id', 'menuitems', 'id', array('delete' => 'CASCADE', 'update' => 'CASCADE'))
            ->create();

        $this->table('orders', array('collation' => 'utf8mb4_general_ci'))
            ->addColumn('user_id', 'integer', array('null' => true))
            ->addColumn('supplier_id', 'integer')
            ->addColumn('total_amount', 'decimal', array('precision' => 8, 'scale' => 2, 'default' => 0))
            ->addColumn('paid_amount', 'decimal', array('precision' => 8, 'scale' => 2, 'null' => true))
            ->addColumn('calculated_balance', 'decimal', array('precision' => 10, 'scale' => 4, 'null' => true))
            ->addColumn('real_balance', 'decimal', array('precision' => 8, 'scale' => 2, 'null' => true))
            ->addColumn('is_ordered', 'boolean', array('default' => false))
            ->addColumn('is_closed', 'boolean', array('default' => false))
            ->addColumn('ordered_at', 'timestamp', array('null' => true))
            ->addColumn('closed_at', 'timestamp', array('null' => true))
            ->addColumn('reopened_at', 'timestamp', array('null' => true))
            ->addTimestamps()
            ->addForeignKey('user_id', 'users', 'id', array('delete' => 'SET_NULL', 'update' => 'CASCADE'))
            ->addForeignKey('supplier_id', 'suppliers', 'id', array('delete' => 'RESTRICT', 'update' => 'CASCADE'))
            ->create();

        $this->table('orderitems', array('collation' => 'utf8mb4_general_ci'))
            ->addColumn('order_id', 'integer')
            ->addColumn('user_id', 'integer')
            ->addColumn('menuitem_id', 'integer', array('null' => true))
            ->addColumn('number', 'string', array('limit' => 30))
            ->addColumn('description', 'string')
            ->addColumn('price', 'decimal', array('precision' => 5, 'scale' => 2))
            ->addColumn('paid_price', 'decimal', array('precision' => 7, 'scale' => 4, 'null' => true))
            ->addTimestamps()
            ->addForeignKey('order_id', 'orders', 'id', array('delete' => 'RESTRICT', 'update' => 'CASCADE'))
            ->addForeignKey('user_id', 'users', 'id', array('delete' => 'RESTRICT', 'update' => 'CASCADE'))
            ->addForeignKey('menuitem_id', 'menuitems', 'id', array('delete' => 'SET_NULL', 'update' => 'CASCADE'))
            ->create();

        $this->table('orderitem_modifiers', array('id' => false, 'primary_key' => array('orderitem_id', 'modifier_id'), 'collation' => 'utf8mb4_general_ci'))
            ->addColumn('orderitem_id', 'integer')
            ->addColumn('modifier_id', 'integer')
            ->addForeignKey('orderitem_id', 'orderitems', 'id', array('delete' => 'CASCADE', 'update' => 'CASCADE'))
            ->addForeignKey('modifier_id', 'menuitems', 'id', array('delete' => 'CASCADE', 'update' => 'CASCADE'))
            ->create();
    }
}
