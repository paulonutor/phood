<?php

use Phinx\Migration\AbstractMigration;

class UserApproval extends AbstractMigration
{
    public function change()
    {
        $this->table('users')
            ->addColumn('is_approved', 'boolean', array('default' => false, 'after' => 'is_hidden'))
            ->update();
    }
}
