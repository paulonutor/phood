$(".nav-toggle").click(function() {
    $(this).next(".nav-menu").toggleClass("is-active");
});

$(".flashes .notification").on("click", ".delete", function() {
    var $notification = $(this).closest(".notification");

    $notification.fadeOut(300, function() {
        $notification.remove();
    });
});

$($(".flashes .notification").get().reverse()).each(function(idx) {
    var $notification = $(this);

    setTimeout(function() {
        $notification.fadeOut(300, function() {
            $notification.remove();
        });
    }, 3000 + (idx * 400));
});
